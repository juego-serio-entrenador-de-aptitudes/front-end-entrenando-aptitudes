import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReglaDeTresPageRoutingModule } from './regla-de-tres-routing.module';

import { ReglaDeTresPage } from './regla-de-tres.page';
import { GameBarModule } from '../../../../shared/game-bar/game-bar.module';
import { QuestionService } from '../../../../services/question/question.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReglaDeTresPageRoutingModule,
    GameBarModule,
    HttpClientModule,
  ],
  declarations: [ReglaDeTresPage],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
  ],
  providers: [
    QuestionService,
  ]
})
export class ReglaDeTresPageModule {}
