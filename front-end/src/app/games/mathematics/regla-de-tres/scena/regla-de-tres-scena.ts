import * as Phaser from 'phaser';

export class ReglaDeTresScene extends Phaser.Scene {
    constructor() {
        super({
            key: 'regla-de-tres'
        });
    }

    preload() {
        const imgRoute = '../../../../../assets/regla-de-tres/';
        this.load.image('background', imgRoute + 'background.png');
    }

    create() {
        this.add.image(512, 288, 'background');
    }

    update() {

    }
}
