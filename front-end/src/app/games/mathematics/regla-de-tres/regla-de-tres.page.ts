import { Component, OnInit } from '@angular/core';
import { ReglaDeTresScene } from './scena/regla-de-tres-scena';

@Component({
  selector: 'app-regla-de-tres',
  templateUrl: './regla-de-tres.page.html',
  styleUrls: ['./regla-de-tres.page.scss'],
})
export class ReglaDeTresPage implements OnInit {

  padding;
  points = 500;
  lifes = 3;
  progress = 0;
  initialize: boolean;

  game = {
    scale: {
      mode: Phaser.Scale.ENVELOP,
      autoCenter: Phaser.Scale.CENTER_BOTH,
      width: 1024,
      height: 576
    },
    type: Phaser.AUTO,
    scene: ReglaDeTresScene,
    physics: {
      default: 'arcade',
      arcade: {
        gravity: { y: 300 },
        debug: false
      }
    },
    instance: null
  };

  constructor() { }

  ngOnInit() {
    this.initialize = true;
  }

}
