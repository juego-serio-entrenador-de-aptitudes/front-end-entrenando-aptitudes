import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReglaDeTresPage } from './regla-de-tres.page';

const routes: Routes = [
  {
    path: '',
    component: ReglaDeTresPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReglaDeTresPageRoutingModule {}
