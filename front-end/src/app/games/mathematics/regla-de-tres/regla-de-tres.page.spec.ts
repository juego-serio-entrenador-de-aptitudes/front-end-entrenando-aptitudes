import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReglaDeTresPage } from './regla-de-tres.page';

describe('ReglaDeTresPage', () => {
  let component: ReglaDeTresPage;
  let fixture: ComponentFixture<ReglaDeTresPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReglaDeTresPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReglaDeTresPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
