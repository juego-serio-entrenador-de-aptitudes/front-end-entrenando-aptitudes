import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AgilidadNumericaPageRoutingModule } from './agilidad-numerica-routing.module';

import { AgilidadNumericaPage } from './agilidad-numerica.page';
import { GameBarModule } from '../../../../shared/game-bar/game-bar.module';
import { QuestionService } from '../../../../services/question/question.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AgilidadNumericaPageRoutingModule,
    GameBarModule,
    HttpClientModule,
  ],
  declarations: [AgilidadNumericaPage],
  providers: [QuestionService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AgilidadNumericaPageModule {}
