import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AgilidadNumericaPage } from './agilidad-numerica.page';

const routes: Routes = [
  {
    path: '',
    component: AgilidadNumericaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AgilidadNumericaPageRoutingModule {}
