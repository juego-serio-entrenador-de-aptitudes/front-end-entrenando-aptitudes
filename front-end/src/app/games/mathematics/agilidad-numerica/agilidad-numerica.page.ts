import { Component, OnInit } from '@angular/core';
import { QuestionService } from '../../../../services/question/question.service';

@Component({
  selector: 'app-agilidad-numerica',
  templateUrl: './agilidad-numerica.page.html',
  styleUrls: ['./agilidad-numerica.page.scss'],
})
export class AgilidadNumericaPage implements OnInit {

  padding;
  points = 500;

  lifes = 3;

  progress = 0;

  constructor(
    private _preguntaService: QuestionService,
  ) { }

  ngOnInit() {

  }

  quitarVida() {
    this.lifes -= 1;
  }

  onClick() {
    console.log('hola');
    this.progress += 10;
  }

}
