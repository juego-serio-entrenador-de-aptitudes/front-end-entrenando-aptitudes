import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AgilidadNumericaPage } from './agilidad-numerica.page';

describe('AgilidadNumericaPage', () => {
  let component: AgilidadNumericaPage;
  let fixture: ComponentFixture<AgilidadNumericaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgilidadNumericaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AgilidadNumericaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
