import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  {
    path: 'agilidad-numerica',
    loadChildren: () => import('./games/mathematics/agilidad-numerica/agilidad-numerica.module').then( m => m.AgilidadNumericaPageModule)
  },
  {
    path: 'regla-de-tres',
    loadChildren: () => import('./games/mathematics/regla-de-tres/regla-de-tres.module').then( m => m.ReglaDeTresPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
