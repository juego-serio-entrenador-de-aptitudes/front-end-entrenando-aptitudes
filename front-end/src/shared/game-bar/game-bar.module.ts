import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GameBarComponent } from './game-bar/game-bar.component';
import { IonicModule } from '@ionic/angular';
import { ProgressImgComponent } from './progress-img/progress-img.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    GameBarComponent,
    ProgressImgComponent,
  ],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
  ],
  exports: [
    GameBarComponent
  ],
})
export class GameBarModule { }
