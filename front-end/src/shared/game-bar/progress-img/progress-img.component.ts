import { Component, OnInit, Input, OnChanges, DoCheck } from '@angular/core';
import anime from 'animejs';

@Component({
  selector: 'app-progress-img',
  templateUrl: './progress-img.component.html',
  styleUrls: ['./progress-img.component.scss'],
})
export class ProgressImgComponent implements OnChanges, OnInit {

  @Input() progress = 0;

  progressLog = '0';
  lastProgress = 0;
  actualProgress = 0;

  constructor() { }

  ngOnChanges(changes) {
    this.changeProgress();
  }

  ngOnInit() {
    this.changeProgress();
  }

  changeProgress() {
    if ( this.progress <= 100 ) {
      this.lastProgress = this.actualProgress;
      this.actualProgress = this.progress;
      anime({
        targets: '.cuadro',
        width: this.actualProgress + '%',
        easing: 'easeInOutQuad',
        direction: 'normal',
        duration: 1000,
        // loop: true
        update: (anim) => {
          this.progressLog = Math.round(anim.progress * (this.actualProgress - this.lastProgress) / 100 + this.lastProgress) + '%';
        }
      });
    }
  }
}
