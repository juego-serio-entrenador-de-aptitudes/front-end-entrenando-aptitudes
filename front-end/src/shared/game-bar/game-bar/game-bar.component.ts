import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { timer, Observable } from 'rxjs';


@Component({
  selector: 'app-game-bar',
  templateUrl: './game-bar.component.html',
  styleUrls: ['./game-bar.component.scss'],
})
export class GameBarComponent implements OnInit {

  @Input() padding = '';

  @Input() lifes: number;

  @Input() time: { seconds: number };

  @Input() lifesActived = false;

  @Input() timeActived = false;

  @Input() progress = 0;

  @Input() totalProgress: number;

  @Input() points = 0;

  @Output() pause = new EventEmitter();

  actualTime: string;

  chronometer: Observable<number>;

  opacity = 0.1;

  constructor() { }

  ngOnInit() {
    if (this.timeActived) {
      this.beginChronometer();
    }

    if ( this.padding === '' ) {
      console.log('calculadon padding');
      this.calculatePadding();
    }
  }

  pausedPressed() {
    this.pause.emit();
  }

  beginChronometer() {
    this.chronometer = timer(0, 1000);
    this.chronometer.subscribe(
      (x) => {
        const timeLeft = this.time.seconds - x;

        this.actualTime = `${Math.trunc(timeLeft / 60)}:${timeLeft % 60}`;
      }
    );
  }

  calculatePadding() {
    const width = Math.max(
      document.body.scrollWidth,
      document.documentElement.scrollWidth,
      document.body.offsetWidth,
      document.documentElement.offsetWidth,
      document.documentElement.clientWidth
    );

    const height = Math.max(
      document.body.scrollHeight,
      document.documentElement.scrollHeight,
      document.body.offsetHeight,
      document.documentElement.offsetHeight,
      document.documentElement.clientHeight
    );

    this.padding = (((width / height) - (16 / 9)) / 2) * width + 'px';

  }
}
