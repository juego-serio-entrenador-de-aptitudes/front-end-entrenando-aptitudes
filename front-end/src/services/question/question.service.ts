import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { GAME_ID } from '../../definitions/gama-ids.definition';
import { QuestionInterface } from '../../interfaces/question.interface';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {
  moduleName = 'question';

  constructor(
    private _http: HttpClient,
  ) { }

  getRandomQuestions(gameId: GAME_ID, quantity: number): Observable<QuestionInterface> {
    const url = environment.backEndUrl + this.moduleName + '/random';
    const body = {
      gameId,
      quantity
    };
    return this._http.post<QuestionInterface>(url, body);
  }

  getPredefinedQuestions() {
    return 'hola';
  }
}
