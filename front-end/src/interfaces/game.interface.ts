import {QuestionInterface} from './question.interface';

export interface GameInterface {
    subject: string;
    name: string;
    questions?: QuestionInterface[];
    records?;
}
